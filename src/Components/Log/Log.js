import React, {Component} from 'react';
import './Log.css'

class Log extends Component {
    render() {
        return(
            <div className='btn'>
                <div className='btn-nav'>
                    <div className='btn-top'>
                        <button>Посещение и оплата</button>
                        <button>Данные спортсмена</button>
                        <button>Особые отметки</button>
                    </div>
                    <input type='text' placeholder='ПОИСК'></input>
                    <div className='btn-bottom'>
                        <button>Группа</button>
                        <button>Графики</button>
                        <button>Настроить журнал</button>
                    </div>
                </div>
                <hr />
                <div className='log-text'>
                    <h1>
                        Данные Журнала
                    </h1>
                </div>
            </div>
        )
    }
}

export default Log