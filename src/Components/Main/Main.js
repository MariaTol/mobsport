import React, {Component} from 'react';
import './Main.css';
import {Switch} from 'react-router-dom';
import {Route} from 'react-router-dom';
import Lenta from '../Lenta/Lenta.js';
import Log from '../Log/Log.js';
import Search from '../Search/Search.js';
import Team from '../Team/Team.js';
import Profile from '../Profile/Profile.js';


class Main extends Component {
    render() {
        return(
            <main className="main">
                <Switch>
                    <Route exact path='/' component={Lenta}/>
                    <Route path='/search' component={Search}/>
                    <Route path='/team' component={Team}/>
                    <Route path='/log' component={Log}/>
                    <Route path='/profile' component={Profile}/>
                </Switch>
          </main>
        )
    }
}

export default Main