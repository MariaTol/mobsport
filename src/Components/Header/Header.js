import React, { Component } from 'react';
import './Header.css';
import menu from '../icon/menu.png';
class Header extends Component {
    render() {
        const body = <a href="#"><img src={menu} alt="menu" /></a>
        return (
            <header className="header">
                {body}
            </header>
        )
    }
}

export default Header