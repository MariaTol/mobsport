import React, {Component} from 'react';
import './Footer.css';
import lenta from '../icon/lenta.png';
import search from '../icon/search.png';
import team from '../icon/team.png';
import log from '../icon/log.png';
import profile from '../icon/profile.png';
import {Link} from 'react-router-dom';

class Footer extends Component {
    render() {
        return(
            <footer className="footer">
                <Link to='/'><img src={lenta} alt="lenta" /></Link>
                <Link to='/search'><img src={search} alt="search" /></Link>
                <Link to='/team'><img src={team} alt="team" /></Link>
                <Link to='/log'><img src={log} alt="log" /></Link>
                <Link to='/profile'><img src={profile} alt="profile" /></Link>
            </footer>
        )
    }
}

export default Footer;